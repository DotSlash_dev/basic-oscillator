package com.dotslash.triple_oscillator;

public class EmptyOscillator extends Oscillator
{
    public EmptyOscillator(int sampleRate)
    {
        super(sampleRate);
        this.type = WaveType.Empty;
    }

    public EmptyOscillator(Oscillator old)
    {
        super(old);
        this.type = WaveType.Empty;
    }

    public void generate()
    {
        for (byte data : this.buffer)
        {
            data = (byte) 0;
        }
    }
}


