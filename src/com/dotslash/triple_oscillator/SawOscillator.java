package com.dotslash.triple_oscillator;

public class SawOscillator extends Oscillator
{
    public SawOscillator(int sampleRate)
    {
        super(sampleRate);
        this.type = WaveType.Saw;
    }

    public SawOscillator(Oscillator old)
    {
        super(old);
        this.type = WaveType.Saw;
    }

    public void generate()
    {
        float currentValue = 0;
        for (int i = 0; i < this.buffSize; i++)
        {
            this.buffer[i] = (byte) (currentValue * volume);
            currentValue += 0x100 / (float)this.waveLength;
        }
    }
}

