package com.dotslash.triple_oscillator;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.util.Arrays;

public class TripleOscillatorActivity extends Activity
{
    byte[] buffer;
    int buffSize;
    Note note;
    int noteSize;
    final int sampleRate = 48000;
    AudioTrack sound;
    boolean state = false;
    Oscillator osc1;

    private View.OnTouchListener playSound = new View.OnTouchListener()
    {
        private Note.Label getNoteLabel(int keyId)
        {
            switch (keyId)
            {
                case R.id.c:
                case R.id.cSharp:
                    return Note.Label.C;
                case R.id.d:
                    return Note.Label.D;
                case R.id.eFlat:
                case R.id.e:
                    return Note.Label.E;
                case R.id.f:
                case R.id.fSharp:
                    return Note.Label.F;
                case R.id.g:
                    return Note.Label.G;
                case R.id.aFlat:
                case R.id.a:
                    return Note.Label.A;
                case R.id.bFlat:
                case R.id.b:
                    return Note.Label.B;
            }
            return null;
        }

        private Note.Alteration getNoteAlteration(int keyId)
        {
            switch (keyId)
            {
                case R.id.c:
                case R.id.d:
                case R.id.e:
                case R.id.f:
                case R.id.g:
                case R.id.a:
                case R.id.b:
                    return Note.Alteration.Natural;

                case R.id.fSharp:
                case R.id.cSharp:
                    return Note.Alteration.Sharp;

                case R.id.eFlat:
                case R.id.aFlat:
                case R.id.bFlat:
                    return Note.Alteration.Flat;
            }
            return null;
        }

        public boolean onTouch(View key, MotionEvent event)
        {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
            {
                Note.Label notePlayed = getNoteLabel(key.getId());
                Note.Alteration noteAlt = getNoteAlteration(key.getId());
                note.setLabel(notePlayed);
                note.setAlteration(noteAlt);
                generate();
                sound.play();
            }
            else if (event.getAction() == MotionEvent.ACTION_UP)
                sound.pause();
            return true;
        }
    };

    private SeekBar.OnSeekBarChangeListener fineTuning = new SeekBar.OnSeekBarChangeListener()
    {
        public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser)
        {
            note.setFine(progress - seekbar.getMax() / 2);
            generate();
        }

        public void onStartTrackingTouch(SeekBar seekbar) {}
        public void onStopTrackingTouch(SeekBar seekbar) {}
    };

    private SeekBar.OnSeekBarChangeListener volumeModifier = new SeekBar.OnSeekBarChangeListener()
    {
        public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser)
        {
            osc1.setVolume(progress / 100f);
            generate();
        }

        public void onStartTrackingTouch(SeekBar seekbar) {}
        public void onStopTrackingTouch(SeekBar seekbar) {}
    };

    private void initSound()
    {
        int config = AudioFormat.CHANNEL_OUT_STEREO;
        int format = AudioFormat.ENCODING_PCM_8BIT;
        this.buffSize = AudioTrack.getMinBufferSize(sampleRate, config, format);
        this.buffer = new byte[buffSize];
        for (byte data : this.buffer) { data = 0; }
        this.sound = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, config, format, buffSize, AudioTrack.MODE_STATIC);
        this.sound.setPlaybackHeadPosition(0);
        this.sound.setStereoVolume(1,1);
    }

    private void initKeyboard()
    {
        ViewGroup keyboard = (ViewGroup) findViewById(R.id.keyboard);
        int childrenNb = keyboard.getChildCount();
        for (int it = 0; it < childrenNb; it++)
        {
            View child = keyboard.getChildAt(it);
            child.setOnTouchListener(playSound);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        SeekBar volume = (SeekBar) findViewById(R.id.volume);
        volume.setOnSeekBarChangeListener(volumeModifier);

        SeekBar fine = (SeekBar) findViewById(R.id.fineTuneSeekBar);
        fine.setOnSeekBarChangeListener(fineTuning);

        initKeyboard();
        initSound();

        if (savedInstanceState == null)
        {
            this.note = new Note(Note.Label.A, Note.Alteration.Natural, 3);
            this.noteSize = (int)(this.sampleRate / this.note.getFrequency());
            genEmpty(null);
            osc1.setVolume((float) volume.getProgress() / 100f);
        }
        else
        {
            this.note = new Note(Note.Label.A, Note.Alteration.Natural, savedInstanceState.getInt("Octave"));
            this.noteSize = (int)(this.sampleRate / this.note.getFrequency());
            switch (savedInstanceState.getInt("Type"))
            {
                case 1:
                    genSine(null);
                    break;
                case 2:
                    genSquare(null);
                    break;
                case 3:
                    genSaw(null);
                    break;
                default:
                    genEmpty(null);
                    break;
            }
            fine.setProgress(savedInstanceState.getInt("Fine"));
            volume.setProgress(savedInstanceState.getInt("Volume"));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state)
    {
        SeekBar volume = (SeekBar) findViewById(R.id.volume);
        SeekBar fine = (SeekBar) findViewById(R.id.fineTuneSeekBar);

        state.putInt("Type", this.osc1.getWaveType().getValue());
        state.putInt("Volume", volume.getProgress());
        state.putInt("Octave", this.note.getOctave());
        state.putInt("Fine", fine.getProgress());
        System.out.println("Stored " + fine.getProgress());
    }

    private void generate()
    {
        if (this.sound.getState() == AudioTrack.STATE_INITIALIZED)
        {
            this.state = false;
            this.sound.pause();
        }
        osc1.setNote(this.note);
        int length = osc1.getLength();
        this.sound.setPlaybackHeadPosition(0);
        int err = sound.setLoopPoints(0, length / 2, -1);
        osc1.generate();
        sound.write(osc1.getWave(), 0, length);
    }

    public void genSine(View view)
    {
        if (osc1 != null)
            osc1 = new SineOscillator(osc1);
        else
            osc1 = new SineOscillator(sampleRate);
        generate();
    }

    // public void genTriangle(View view)
    // {
    //     if (osc1 != null)
    //         osc1 = new TriangleOscillator(osc1);
    //     else
    //         osc1 = new TriangleOscillator(sampleRate);
    //     generate();
    // }

    public void genSquare(View view)
    {
        if (osc1 != null)
            osc1 = new SquareOscillator(osc1);
        else
            osc1 = new SquareOscillator(sampleRate);
        generate();
    }

    public void genSaw(View view)
    {
        if (osc1 != null)
            osc1 = new SawOscillator(osc1);
        else
            osc1 = new SawOscillator(sampleRate);
        generate();
    }

    public void genEmpty(View view)
    {
        if (osc1 != null)
            osc1 = new EmptyOscillator(osc1);
        else
            osc1 = new EmptyOscillator(sampleRate);
        generate();
    }

    @Override
    public void onPause()
    {
        this.state = false;
        this.sound.pause();
        super.onPause();
    }

    public void onDestroy()
    {
        sound.release();
        super.onDestroy();
    }

    public void octUp(View button)
    {
        note.setOctave(note.getOctave() + 1);
    }

    public void octDown(View button)
    {
        note.setOctave(note.getOctave() - 1);
    }

    public void fineReset(View button)
    {
        SeekBar fine = (SeekBar) findViewById(R.id.fineTuneSeekBar);
        fine.setProgress(fine.getMax() / 2);
    }
}
