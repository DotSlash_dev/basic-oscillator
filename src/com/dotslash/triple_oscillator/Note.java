package com.dotslash.triple_oscillator;

public class Note
{
    public enum Label
    {
        C(0),
        D(2),
        E(4),
        F(5),
        G(7),
        A(9),
        B(11);

        int value;
        private Label(int value) { this.value = value; }
        public int getValue() { return this.value; }
        public Label next()
        {
            switch (this)
            {
                case A: return B;
                case B: return C;
                case C: return D;
                case D: return E;
                case E: return F;
                case F: return G;
                case G: return A;
            }
            return null;
        }
        public Label prev()
        {
            switch (this)
            {
                case A: return G;
                case B: return A;
                case C: return B;
                case D: return C;
                case E: return D;
                case F: return E;
                case G: return F;
            }
            return null;
        }
    }
    public enum Alteration
    {
        Flat(-1),
        Natural(0),
        Sharp(1);

        int value;
        private Alteration(int value) { this.value = value; }
        public int getValue() { return this.value; }
    }

    Label label;
    Alteration alt;
    int octave;
    int fine;

    public Note(Label label, Alteration alt, int octave)
    {
        this.label = label;
        this.alt = alt;
        this.octave = octave;
    }

    public Label getLabel() { return label; }
    public Alteration getAlteration() { return alt; }
    public int getOctave() { return octave; }
    public int getFine() { return fine; }

    public void setLabel(Label label) { this.label = label; }
    public void setAlteration(Alteration alt) { this.alt = alt; }
    public void setOctave(int octave) { this.octave = octave; }
    public void setFine(int fine) { this.fine = fine; }

    public float getFrequency()
    {
        float freqA0 = 27.5f; // Note: A, Octave #0
        int noteA0 = 9;
        float realNote = this.octave * 12 + this.label.getValue() + this.alt.getValue() + this.fine / 64f;
        float realFreq = (float) Math.pow(2, (realNote - noteA0) / 12.0);
        return realFreq * freqA0;
    }

    public void next()
    {
        switch (this.alt)
        {
            case Flat:
                this.alt = Alteration.Natural;
                break;
            case Natural:
                if (this.label == Label.E || this.label == Label.B)
                {
                    this.label = this.label.next();
                    if (this.label == Label.C)
                        this.octave++;
                }
                else
                    this.alt = Alteration.Sharp;
                break;
            default:
                this.alt = Alteration.Natural;
                this.label = this.label.next();
        }
        System.out.println(this.label);
    }

    public void prev()
    {
        switch (this.alt)
        {
            case Sharp:
                this.alt = Alteration.Natural;
                break;
            case Natural:
                if (this.label == Label.F || this.label == Label.C)
                {
                    this.label = this.label.prev();
                    if (this.label == Label.B)
                        this.octave--;
                }
                else
                    this.alt = Alteration.Flat;
                break;
            default:
                this.alt = Alteration.Natural;
                this.label = this.label.prev();
        }
    }
}
