package com.dotslash.triple_oscillator;

import java.util.Arrays;

public class SquareOscillator extends Oscillator
{
    public SquareOscillator(int sampleRate)
    {
        super(sampleRate);
        this.type = WaveType.Square;
    }

    public SquareOscillator(Oscillator old)
    {
        super(old);
        this.type = WaveType.Square;
    }

    public void generate()
    {
        boolean sign = true;
        int halfWavePos = (int)(this.waveLength / 2.0);
        for (int i = 0; i < this.buffSize; i++)
        {
            if (i % halfWavePos == halfWavePos - 1)
                sign = !sign;
           this.buffer[i] = (byte)((sign ? 1 : -1) * 0x7f * (1.01 - volume));
        }
    }
}

