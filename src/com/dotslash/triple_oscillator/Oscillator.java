package com.dotslash.triple_oscillator;

import java.util.Arrays;

import android.media.AudioFormat;
import android.media.AudioTrack;

public abstract class Oscillator
{
    public enum WaveType{
        Empty(0),
        Sine(1),
        Square(2),
        Saw(3);

        int value;
        private WaveType(int value) { this.value = value; }
        public int getValue() { return this.value; }
    }

    int sampleRate;
    WaveType type;
    byte[] buffer;
    int buffSize;
    int waveLength;
    Note note;
    float volume;

    public Oscillator(int sampleRate)
    {
        int config = AudioFormat.CHANNEL_OUT_STEREO;
        int format = AudioFormat.ENCODING_PCM_8BIT;
        this.sampleRate = sampleRate;
        this.buffSize = AudioTrack.getMinBufferSize(sampleRate, config, format);
        this.buffer = new byte[buffSize];
    }

    public Oscillator(Oscillator old)
    {
        int config = AudioFormat.CHANNEL_OUT_STEREO;
        int format = AudioFormat.ENCODING_PCM_8BIT;
        this.sampleRate = old.sampleRate;
        this.buffSize = AudioTrack.getMinBufferSize(sampleRate, config, format);
        this.buffer = new byte[buffSize];
        this.note = old.note;
        this.volume = old.volume;
    }

    public WaveType getWaveType() { return this.type; }
    public byte[] getWave() { return Arrays.copyOf(this.buffer, waveLength); }
    public int getLength() { return this.waveLength; }
    public Note getNote() { return this.note; }
    public int getSampleRate() { return this.sampleRate; }
    public float getVolume() { return this.volume; }

    public void setNote(Note note)
    {
        this.note = note;
        this.waveLength = (int) (this.sampleRate / this.note.getFrequency());
    } 
    public void setVolume(float volume) { this.volume = volume; }
    public void getWaveType(WaveType type) { this.type = type; }

    public abstract void generate();
}
