package com.dotslash.triple_oscillator;

public class SineOscillator extends Oscillator
{
    public SineOscillator(int sampleRate)
    {
        super(sampleRate);
        this.type = WaveType.Sine;
    }

    public SineOscillator(Oscillator old)
    {
        super(old);
        this.type = WaveType.Sine;
    }

    public void generate()
    {
        float value;
        for (int i = 0; i < this.buffSize; i++)
        {
            value = (float) (Math.sin(Math.PI * i / (float)this.waveLength) * 0x7f);
            this.buffer[i] = (byte) (value * volume);
        }
    }
}

